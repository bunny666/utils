<?php

namespace Bunny666\Utils;

class ResponseUtil
{
    /**
     * 返回失败的json数据.
     *
     * @param string $msg
     * @param mixed $data
     * @return string
     */
    public static function responseSuccess(mixed $data = [],string $msg = 'success') :string{
        return json_encode([
            'status'=>true,
            'msg'=>$msg,
            'data'=>$data,
        ],JSON_UNESCAPED_UNICODE);
    }

    /**
     * 返回成功的json数据.
     *
     * @param string $msg
     * @param mixed $data
     * @return string
     */
    public static function responseError(string $msg = 'error',mixed $data = []) :string{
        return json_encode([
            'status'=>false,
            'msg'=>$msg,
            'data'=>$data,
        ],JSON_UNESCAPED_UNICODE);
    }
}